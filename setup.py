#!/usr/bin/env python
# coding=utf-8
from distutils.core import setup

setup(
    name        = 'pydsh',
    version     = '0.1',
    description = 'PYthon Domain-Specific sHell',
    author      = 'Alexey Polyakov',
    packages = [
        'pydsh',
        'pydsh.client',
        'pydsh.command',
        'pydsh.commons',
        'pydsh.grammar',
        'pydsh.server',
        'pydsh.util',
        'pydsh.shell',
        'pydsh.process',
        'pydsh.plugins',
    ]
)
