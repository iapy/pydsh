# coding=utf-8
import os, sys, argparse

if __name__ == '__main__':
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)
        ))
    ))

    from server import PyDSHSSLServer

    parser = argparse.ArgumentParser(prog = "server")
    parser.add_argument("--port",  help = "port to listen", type = int, default = 12345)
    parser.add_argument("package", help = "package to import")

    args = parser.parse_args()

    def _error(s):
        print >> sys.stderr, s
        sys.exit(1)

    try:
        dir, package = os.path.split(os.path.abspath(os.path.expanduser(args.package)))
        os.sys.path.append(dir)

        p, certs, shell = __import__(package), None, None
        if "SSL_SERVER_CERTIFICATES" not in p.__dict__:
            _error("Cannot find server certificates. Module should define\nSSL_SERVER_CERTIFICATES = pydsh.server.ServerSertificates(...)")
        else:
            certs = p.__dict__["SSL_SERVER_CERTIFICATES"]

        if "PYDSH_SHELL" not in p.__dict__:
            _error("Cannot find server implementation. Module should define\nclass PYDSH_SHELL")
        else:
            shell = p.__dict__["PYDSH_SHELL"]

        PyDSHSSLServer(shell, args.port, certs).serve()
    except ImportError as e:
        print >> sys.stderr, e.message

