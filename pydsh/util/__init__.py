# coding=utf-8
import codecs, escape, guard

__all__ = guard.ImportGuard()
codecs.register(escape.__lookup_escape_codec)