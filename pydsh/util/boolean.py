# coding=utf-8
import operator

class Not(list):
    def __init__(self):
        list.__init__(self)
        self.flag = False

    def add(self, x):
        if x == "!":
            self.flag = True
        elif self.flag:
            def _exec():
                return not x()
            self.append(_exec)
            self.flag = False
        else:
            self.append(x)
        return self

class AndOr(list):
    class Operator:
        def __init__(self, l, r, o):
            self.l, self.r, self.o = l, r, o

        def execute(self):
            return self.o(self.l(),self.r())

    def make_op(self, a, b):
        return AndOr.Operator(a,b,self.op).execute

    def __init__(self, op, st):
        list.__init__(self)
        self.flag = False
        self.stack = []
        self.op, self.st = op, st

    def add(self, x):
        if len(self.stack) == 2:
            self.append(self.make_op(self.stack[0],x))
            self.stack = []
        elif type(x) == unicode or type(x) == str:
            if x == self.st:
                self.stack.append(x)
            else:
                self += self.stack
                self.stack = []
                self.append(x)
        else:
            self += self.stack
            self.stack = [x]
        return self

    def done(self):
        self += self.stack
        return self


class And(AndOr):
    def __init__(self):
        AndOr.__init__(self, operator.and_, "&&")

class Or(AndOr):
    def __init__(self):
        AndOr.__init__(self, operator.or_, "||")