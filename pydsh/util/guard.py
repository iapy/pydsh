# coding=utf-8

class ImportGuard(object):
    def __getitem__(self, item):
        raise ImportError("Cannot import everything from module")