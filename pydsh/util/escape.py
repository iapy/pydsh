# coding=utf-8
import codecs
from array import array

_closing = {')':'(','}':'{',']':'['}
_opening = {'(':')','{':'}','[':']'}
_escaper = '\\'
_escapes = {
    'n'  : u'\n',
    't'  : u'\t',
    'r'  : u'\r',
    '"'  : u'"',
    '\\' : u'\\' ,
    '['  : u'[' ,
    ']'  : u']',
    '('  : u'(',
    ')'  : u')',
    '{'  : u'{',
    '}'  : u'}',
    '='  : u'=',
    ':'  : u':',
    '!'  : u'!'
}

def _encode(_):
    pass

def _decode(value):
    prev, copy = 0, array('u')
    for i in range(0,len(value)):
        if value[i] == _escaper:
            if prev:
                copy.append(value[i])
                prev = False
            else:
                prev = True
        elif prev:
            copy.append(_escapes[value[i]])
            prev = False
        else:
            copy.append(value[i])
    return copy.tounicode(),len(copy)

def _balance(value):
    brackets, previous, quotes = {'(':0,'{':0,'[':0}, None, False
    for i in range(0,len(value)):
        if value[i] in brackets.keys() and previous != _escaper:
            brackets[value[i]] += 1
        elif value[i] in _closing.keys() and previous != _escaper:
            brackets[_closing[value[i]]] -= 1
        previous = value[i]
    return sum([v for _,v in brackets.items()])

class BalancedReader(object):
    def __init__(self, prompt, **_):
        super(BalancedReader, self).__init__()
        self.prompt         = prompt
        self.script         = None
        self.current_prompt = self.prompt

    def _balance(self):
        balance = _balance(self.script)
        if balance < 0:
            self.printer("Unable to parse!", True)
            self.script = None
            self.current_prompt = self.prompt
        elif balance > 0:
            self.current_prompt = " " * len(self.prompt)
        else:
            self.execute()
            self.script, self.current_prompt = None, self.prompt

    def execute(self):
        raise

def __lookup_escape_codec(name):
    if name == "script_escape":
        return codecs.CodecInfo(
            name = 'script_escape',
            encode = _encode,
            decode = _decode
        )
