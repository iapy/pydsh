# coding=utf-8
import types
import grako.exceptions
from collections import namedtuple

class FailedParse(grako.exceptions.FailedParse):
    HookClass = namedtuple('HookClass',['pos'])

    def __init__(self, message):
        grako.exceptions.FailedParse.__init__(self, FailedParse.HookClass(pos = 0), None, message)

    def __str__(self):
        return self.message

def check(value, checker):
    if type(checker) == types.FunctionType or type(checker) == staticmethod:
        assert isinstance(value, tuple)
        return checker(value) if type(checker) == types.FunctionType else checker.__func__(value)
    elif type(checker) == types.TypeType or type(checker) == types.ClassType:
        if not isinstance(value, checker):
            raise ValueError("Wrong type. Expecting [{0}] found [{1}]".format(checker.__name__, type(value).__name__))
    elif type(checker) != list:
        if value != checker:
            raise FailedParse("Expecting [{0}] found [{1}]".format(checker, value))
    else:
        p = set(map(lambda x : type(x) == types.TypeType or type(x) == types.ClassType, checker))
        assert len(p) == 1
        if p.pop():
            if type(value) not in checker:
                raise ValueError("Wrong type. Expecting one of ({0}) found [{1}]".format(",".join([x.__name__ for x in checker]), type(value).__name__))
        else:
            if value not in checker:
                raise FailedParse("Expecting one of {0} found [{1}]".format(",".join([str(x) for x in checker]), str(value)))