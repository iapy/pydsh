# coding=utf-8
import os, sys

class _Wrap(object):
    def __init__(self, function, stdin_file):
        self.function, self.stdin_file = function, stdin_file

    def __call__(self, state, queue, *args):
        sys.stdin = os.fdopen(self.stdin_file)
        self.function(state, *args)
        state.push(queue)

class Interrupt(object):
    instance = None
    def __init__(self, target, state, args):
        import signal
        from multiprocessing import Process, Queue

        Interrupt.instance = self
        self.queue,   self.state = Queue(1), state
        self.process, self.interrupted = Process(target=_Wrap(target, sys.stdin.fileno()), args=(self.state, self.queue,) + args), False
        self.handler = signal.signal(signal.SIGINT, Interrupt._handler)

        self.process.start()
        self.process.join()
        signal.signal(signal.SIGINT, self.handler)

    @staticmethod
    def _handler(_foo,_bar):
        Interrupt.instance.process.terminate()
        Interrupt.instance.interrupted = True

    def merge(self):
        import sys
        if not self.interrupted:
            if not self.queue.empty():
                self.state.merge(self.queue.get())
            else:
                sys.exit(self.process.exitcode)