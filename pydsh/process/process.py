# coding=utf-8
import os, subprocess

def process(p, err, out, cwd = None, env = None):
    with open(os.devnull, 'w') as FNULL:
        sout = FNULL if not out else  subprocess.PIPE
        serr = FNULL if not err else (subprocess.STDOUT if out else subprocess.PIPE)

        result = subprocess.Popen(p, bufsize = 0, cwd = cwd, env = env, stdout = sout, stderr = serr).stdout
        result = iter(result.readline, '') if result else list()

        for line in result:
            yield line.rstrip('\n')
