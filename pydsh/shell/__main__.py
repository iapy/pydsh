# coding=utf-8
import os, sys, argparse

class FileReader(object):
    def __init__(self, file):
        self.file = open(file, 'r')

    def __call__(self, *args, **kwargs):
        line = self.file.readline()
        if not line:
            raise EOFError()
        return line

if __name__ == '__main__':
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)
        ))
    ))

    parser = argparse.ArgumentParser(prog = "shell")
    parser.add_argument("package", help = "package to import")
    parser.add_argument("script",  help = "script to run", default = None, nargs = "?")

    args = parser.parse_args()

    import pydsh

    def _error(s):
        print >> sys.stderr, s
        sys.exit(1)

    try:
        dir, package = os.path.split(os.path.abspath(os.path.expanduser(args.package)))
        os.sys.path.append(dir)

        p, shell = __import__(package), None
        if "PYDSH_SHELL" not in p.__dict__:
            _error("Cannot find server implementation. Module should define\nclass PYDSH_SHELL")
        else:
            shell = p.__dict__["PYDSH_SHELL"]

        input = raw_input

        if args.script:
            input = FileReader(os.path.abspath(os.path.expanduser(args.script)))

        pydsh.Shell(shell()).cmdloop(input=input)
    except ImportError as e:
        print >> sys.stderr, e.message

