#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
grako $DIR/dsl.ebnf > grammar/__init__.py

if [ "$?" == "0" ] && [ "$1" == "test" ]; then
    echo testing...
    python $DIR/__init__.py -t $DIR/test.shl COMMAND
fi
