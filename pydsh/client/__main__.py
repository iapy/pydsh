# coding=utf-8
import os, sys, argparse

if __name__ == '__main__':
    sys.path.append(os.path.dirname(
        os.path.dirname(os.path.dirname(
            os.path.realpath(__file__)
        ))
    ))

    import client

    parser = argparse.ArgumentParser(prog = "client")
    parser.add_argument("host",   help = "host to connect")
    parser.add_argument("cert",   help = "certificate to use")
    parser.add_argument("--port", help = "port to connect", type = int, default = 12345)

    args = parser.parse_args()
    client.PyDSHSSLClient(
        name = args.host,
        cert = args.cert,
        port = args.port
    ).cmdloop()
