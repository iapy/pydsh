from pydsh.util import escape
import socket, ssl, struct, sys

class PyDSHSSLClient(escape.BalancedReader):
    def __init__(self, name, cert, port=12345, dns=None):
        if not dns: dns = {}
        super(PyDSHSSLClient,self).__init__(prompt=(name + " > "))
        if name not in dns:
            self.host = socket.gethostbyname(name)
        else:
            self.host = dns[name]
        self.port, self.cert, self.sock, self.finished = port, cert, None, False

    def cmdloop(self, input=raw_input, redirect=sys.stdout):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.host, self.port))
        self.sock = ssl.wrap_socket(self.sock, certfile = self.cert)
        self.redirect = redirect

        while not self.finished:
            try:
                line = input(self.current_prompt)
                self.script = (line if not self.script else self.script + ("\n" + line)).strip()
                self._balance()
            except EOFError:
                break

    def execute(self):
        if not self.script:
            return
        if self.script == 'exit':
            self.sock.send(struct.pack('I', 0))
            del self.sock
            self.finished = True
        else:
            self.sock.send(struct.pack('I', len(self.script)))
            self.sock.send(self.script)
            data = self.sock.recv(4)
            dlen = struct.unpack('I', data)[0]
            while dlen != 1:
                if dlen != 2:
                    data = self.sock.recv(dlen - 2)
                else:
                    data = ""
                print >> self.redirect, data
                data = self.sock.recv(4)
                dlen = struct.unpack('I', data)[0]
            self.script = None
