# coding=utf-8
import unittest, base
import pydsh.commons as commons
import pydsh.command as command

class TestInterpreterBase(commons.MapMethods):
    @command
    def name(self):
        return "cmd_name"

class TestInterpreter(TestInterpreterBase, commons.TypeCast):
    __prefix = 'cmd_'

    # Commands without named arguments
    @command
    def cmd_env(self, _context):
        for k,v in _context.env.items():
            yield (k,v)

    def cmd_echo(self, *args, **kwargs):
        for a in args:
            yield a

    @command
    def cmd_envecho(self, _context, *args):
        for k,v in _context.env.items():
            yield (k,v)

        for a in args:
            yield a

    @command
    def cmd_envechoa(self, _context, arg, *args):
        for k,v in _context.env.items():
            yield (k,v)

        for a in args:
            yield a

        yield arg

    #Commands with named arguments
    @command(
        command.arg(name = "host", validate = [str, unicode], help = "host to resolve")
    )
    def cmd_printarg(self, host):
        return host

    @command(
        command.arg(name = "user", validate = [str, unicode], help = "username to print")
    )
    def cmd_user(self, _context, user):
        yield user

        for k,v in _context.env.items():
            yield (k,v)

    @command(
        command.arg(name = "host", validate = [str, unicode]),
        command.arg(name = "port", validate = int)
    )
    def cmd_glue(self, host, _context, port = 80):
        yield (host, port)

        for k,v in _context.env.items():
            yield (k,v)

class TestDecorators(base.PyDSHTest):
    def __init__(self, *args, **kwargs):
        super(TestDecorators,self).__init__(TestInterpreter, *args, **kwargs)

    def test_SimpleDecorators(self):
        self.shell.execute("a=int:10")
        self.shell.execute("name")
        self.shell.execute("env")
        self.shell.execute("echo a $a")
        self.shell.execute("envecho a $a b")
        self.shell.execute("envechoa a $a b")
        self.shell.execute("envechoa int:22")
        self.shell.execute("envechoa")
        self.assertOutput([
            "cmd_name",
            (u'a', 10),
             u'a', 10,
            (u'a', 10), u'a', 10, u'b',
            (u'a', 10), 10, u'b', u'a',
            (u'a', 10), 22,
            ('envechoa() takes at least 3 arguments (2 given)', True)
        ])

    def test_ArgumentalDecorators(self):
        self.shell.execute("printarg")
        self.shell.execute("printarg google.com")
        self.shell.execute("printarg int:123")
        self.shell.execute("a=int:12345")
        self.shell.execute("user test")
        self.shell.execute("user float:15.43")
        self.shell.execute("glue google.com")
        self.shell.execute("glue google.com $a")
        self.shell.execute("glue google.com 44")
        self.assertOutput([
            ('printarg() takes exactly 2 arguments (1 given)', True),
            u'google.com',
            ('Wrong type. Expecting one of (str,unicode) found [int]', True),
            u'test', (u'a', 12345),
            ('Wrong type. Expecting one of (str,unicode) found [float]', True),
            (u'google.com', 80), (u'a', 12345),
            (u'google.com', 12345), (u'a', 12345),
            ('Wrong type. Expecting [int] found [unicode]', True),
        ])

if __name__ == "__main__":
    unittest.main()