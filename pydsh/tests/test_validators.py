# coding=utf-8
import unittest, base, pydsh, types

import pydsh.util.errors as errors
import pydsh.commons as commons
import pydsh.command as command

class TestInterpreter(commons.MapMethods, commons.TypeCast):
    __prefix = 'cmd_'

    @staticmethod
    def validate(tp):
        errors.check(tp, types.TupleType)
        if len(tp) != 2:
            raise ValueError("Expecting [else] [str,unicode]")
        errors.check(tp[0], u'else')
        errors.check(tp[1], [str, unicode])
        return 1 #skip

    @command(
        command.arg("condition", validate = pydsh.Condition)
    )
    def cmd_test1(self, condition):
        yield condition()

    @command(
        command.opt("opt",       validate = bool),
        command.arg("condition", validate = pydsh.Condition)
    )
    def cmd_test2(self, condition, opt = False):
        yield condition()
        yield opt

    @command(
        command.opt("opt",       validate = bool),
        command.arg("condition", validate = pydsh.Condition),
        command.arg("iftrue",    validate = [str, unicode]),
        command.arg("iffalse",   validate = validate),
    )
    def cmd_test3(self, condition, iftrue, opt = False, iffalse = None):
        if condition():
            yield iftrue
        elif iffalse:
            yield iffalse
        yield opt

class TestValidators(base.PyDSHTest):
    def __init__(self, *args, **kwargs):
        super(TestValidators,self).__init__(TestInterpreter, *args, **kwargs)

    def test_Validators(self):
        self.shell.execute("test1[int:10>int:8]")
        self.shell.execute("test2[int:10<int:8]")
        self.shell.execute("test2 --opt [int:10>int:8]")
        self.shell.execute("test3 [int:10>int:8] OK")
        self.shell.execute("test3 [int:10>int:8] OK else KO")
        self.shell.execute("test3 [int:10<int:8] OK else KO")
        self.shell.execute("test3 --opt [int:10>int:8] OK")
        self.shell.execute("test3 --opt [int:10>int:8] OK else KO")
        self.shell.execute("test3 --opt [int:10<int:8] OK else KO")
        self.shell.execute("test3 [int:10>int:8] OK els KO")
        self.shell.execute("test3 [int:10<int:8] OK els KO")
        self.shell.execute("test3 --opt [int:10>int:8] OK els KO")
        self.shell.execute("test3 --opt [int:10<int:8] OK els KO")
        self.assertOutput([
            True,
            False, False,
            True,  True,
            u'OK', False,
            u'OK', False,
            u'KO', False,
            u'OK', True,
            u'OK', True,
            u'KO', True,
            ('Expecting [else] found [els]', True),
            ('Expecting [else] found [els]', True),
            ('Expecting [else] found [els]', True),
            ('Expecting [else] found [els]', True),
        ])

if __name__ == "__main__":
    unittest.main()