# coding=utf-8
import unittest, base
import testpackage
import testmodules.barmodule, testmodules.foomodule
import pydsh.commons as commons

class TestInterpreter(commons.TypeCast, commons.MapModules):
    def __init__(self):
        super(TestInterpreter,self).__init__([
            testpackage,
            testmodules.barmodule,
            testmodules.foomodule
        ])

class TestModules(base.PyDSHTest):
    def __init__(self, *args, **kwargs):
        super(TestModules,self).__init__(TestInterpreter, *args, **kwargs)

    def test_Mappings(self):
        self.shell.execute("a=int:10")
        self.shell.execute("b=int:20")
        self.shell.execute("c=test")
        self.shell.execute("help")
        self.shell.execute("bar_echo a b c")
        self.shell.execute("modfoo.env")
        self.shell.execute("foo.test")
        self.assertOutput([
            'help   - Print help message',
            'reload - Reload modules',
            'bar_echo    - test command',
            '',
            'foo.test    - test command',
            '',
            'modfoo.echo - echo env',
            'modfoo.env  - print env',
            u'a', u'b', u'c',
            (u'a',10), (u'b', 20), (u'c',u'test'),
            u'hi there!'
        ])

    def test_Options(self):
        self.shell.execute("a=test")
        self.shell.execute("modfoo.echo test")
        self.shell.execute("modfoo.echo --env test")
        self.assertOutput([
            u'test',
            u'test', (u'a', u'test')
        ])

if __name__ == "__main__":
    unittest.main()