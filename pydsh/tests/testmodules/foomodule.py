# coding=utf-8
import pydsh.command as command
__namespace = 'modfoo'

@command
def env(_context):
    """print env"""
    for k in sorted(_context.env.keys()):
        yield k, _context.env[k]


@command(
    command.opt('env', help = "print environment"),
    command.arg('arg', help = "argument to echo")
)
def echo(arg, _context, env = False):
    """echo env"""
    yield arg
    if env:
        yield globals()['env'].wrapped(_context)