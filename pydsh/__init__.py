# coding=utf-8
from .dsh import Shell
from .dsh import Script
from .dsh import Condition

from util.errors import check
import util.guard

__all__ = util.guard.ImportGuard()
__version__ = (0,1)

# initialization
try:
    import readline
    if 'libedit' in readline.__doc__:
        readline.parse_and_bind("bind -e")
        readline.parse_and_bind("bind '\t' rl_complete")
    else:
        readline.parse_and_bind("tab: complete")
except:
    pass