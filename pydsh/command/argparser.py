# coding=utf-8
import argparse

class ArgumentParser(argparse.ArgumentParser):
    class _Namespace(object):
        def __init__(self):
            self.__opts = dict()
            self.__args = []

        def __setattr__(self, key, value):
            if key in ['_Namespace__opts', '_Namespace__args', 'arguments']:
                super(ArgumentParser._Namespace,self).__setattr__('_Namespace__args' if key == 'arguments' else key, value)
            else:
                self.__opts[key] = value

        def __getattribute__(self, key):
            if key in ['_Namespace__opts', '_Namespace__args', 'arguments']:
                return super(ArgumentParser._Namespace,self).__getattribute__('_Namespace__args' if key == 'arguments' else key)
            elif key in self.__opts:
                return self.__opts[key]
            else:
                raise KeyError()

    def exit(self, status=0, message=None):
        pass

    def error(self, message):
        pass

    def _parse_optional(self, arg_string):
        if type(arg_string) not in [str,unicode]:
            return None
        return super(ArgumentParser,self)._parse_optional(arg_string)

    def parse_args(self, args):
        ns = super(ArgumentParser, self).parse_args(args, ArgumentParser._Namespace())
        return (ns._Namespace__opts, tuple(ns._Namespace__args))