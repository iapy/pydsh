# coding=utf-8
import command, argument

class DecorateFunctionCall(object):
    """Implementation of module *command*"""
    def __call__(self, *args):
        return command.Command(*args)

    @staticmethod
    def nofork(*args):
        result = command.Command(*args)
        setattr(result, '_nofork', True)
        return result

    Command     = command.CommandBase
    arg         = argument.Argument
    opt         = argument.Option
